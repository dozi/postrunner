# README data

## Aim of the package

This is a small R-package which helps to quickly restructure the raw data output from the Shimadzu GCMSPostrun Analysis software.
The raw data structure unfortunately is not very friendly in regards of data analysis and therefore this script is supposed to reformat them in a way
to make data analysis easier.

## Original rawdata structure
![](dummyfiles/rawoutput_postrun.PNG) <br> <br>
The raw data output has the filename/sample written above the actual data and every individual sample is put below each sample.
Therefore, reading in the data is impossible as either the samplename is missing or all columns are considered as 1 as there are no header columns.
To check the file by yourself, you can check the "dummyfiles" folder, where it is stored.

### Prerequisites for the function to work
As you can see in the image above, the file should have the original international data structure. This means:

* Dot as decimal separator (In Germany it's usually comma)
* Comma as list separator (In Germany it's usually semicolon)

Based on these requisites also the following results:
* The compound names are NOT allowed to have commas within
  * e.g. 1,8-Cadinene won't work as it will interpret "1" and "8-Cadinene" as different columns. Replace "," with the word "comma" and later on you can replace it back again
  * Don't use dots as thousand separators or in compound names. Proceed the same way as with commas.
  
As you can see in the picture above, in the header of each file there is the filepath written.
The script takes the name out of THIS filepath to add it as samplename. In order to be able to do so, NO WHITESPACES are allowed and they
should be all replaced by underscores.

For example the filepath: Z:/imaginary filepath 1/data results/sample1.qgd **will not work** <br>
Instead, it would need to be written like this: Z:/imaginary_filepath_1/data_results/sample1.qgd (see the underscores) <br>
Otherwise the samplename will be "NA" and need to be added manually by yourself.
## Restructured format
![](dummyfiles/output_tidied.PNG) <br> <br>
Now, the datafile has the sample written in the first column named "sample". If the samplename convention is well done,
treatment and further metainformation columns can be produced based on the sample name.
The example file is stored in "dummyfiles" if you want to check it yourself.

## Installation
can be easily installed using devtools::install_github("DoZi93/postrunner") in R.

## Application
The package only contains one function "postrunner::restructure()" and takes only one argument which is the filepath.
So an application could look like:

```
data <- postrunner::restructure("D:/results/gcms_output.txt)
```